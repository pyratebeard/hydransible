# Hydransible

Hydransible is an Ansible playbook repository for multiple server types in multiple 'identical' environments.

The design of this repository is based on an post by Jakub Skalecki entitled "Managing multiple environments with Ansible - best practices", the link to which can be found [below](#links).

A useful tutorial on Ansible can also be found in the [links](#links).

## INSTALLATION

1. Install Ansible
2. Clone this repository
3. Install `sshpass`

## USAGE
_For a more detailed explanation see the [How it works](#how-it-works) section_

Run playbook
```
ansible-playbook -i hosts/<site>/inventory playbook.yml
```

Run specify tags
```
ansible-playbook -i hosts/<site>/inventory playbook.yml --tags exampletag
```

To create a new role
```
cd roles/
ansible-galaxy init <role_name>
```

## DETAILS

### Tree
```
hydransible
├── hosts
│   ├── dr
│   │   ├── groups_vars
│   │   │   └── all.yml
│   │   ├── host_vars
│   │   │   └── ...
│   │   ├── inventory
│   │   └── secrets.yml
│   ├── prod
│   │   └── ...
│   ├── systest
│   │   └── ...
│   ├── uat
│   │   └── ...
│   ├── shared_secrets.yml
│   └── shared_vars.yml
├── roles
│   ├── apache
│   │   ├── README.md
│   │   └── ...
│   ├── app
│   │   └── ...
│   ├── bastion
│   │   └── ...
│   ├── common
│   │   └── ...
│   ├── integration
│   │   └── ...
│   └── oracle
│       └── ...
├── tasks
│   └── load_vars.yml
├── ansible.cfg
├── apache.yml
├── app.yml
├── bastion.yml
├── common.yml
├── integration.yml
├── oracle.yml
├── README.yml
└── ssh_config
```

Secrets should be encrypted using:
```
ansible-vault encrypt hosts/shared_secrets.yml
```

### HOW IT WORKS

#### Environment
In order for the dynamic variables to work there are a number of checks that take place within the tasks and templates.  The environment variables are pulled from the relevant inventory file, for example to use UAT you will run the following
```
ansible-playbook -i hosts/uat/inventory apache.yml
```
In each inventory file there is an `env` var and a task to include the relevant environment vars file.  This task uses the `always` tag so you don't need to specify it to be run.

#### Roles
In the inventory file nodes are grouped by role
```
[apache]
apache1
apache2

[app]
app1
app2
app3
app4

[integration]
integration1
integration2

[oracle]
oracle1
oracle2
```

For each role playbook the hosts are set, so for example if you run the apache.yml playbook without specifying a node or group, it will only ever run against the two apache nodes in the chosen environment.  This grouping also means that if you're running the common playbook you can limit to 'apache' to run against both apache nodes or you can limit to a specific node
```
ansible-playbook -i hosts/uat/inventory --limit apache common.yml # will run against apache1 and apache2 in UAT
ansible-playbook -i hosts/uat/inventory --limit integration1 common.yml # will only run against integration1 in UAT
```

Some roles require additional variables to be specified when running the playbook, see the role README for details.

#### Tasks
Playbooks are split into the different roles and one common file which has tasks relevant to all nodes.  All the tasks are in the file '<role>/tasks/main.yml'.  Each task has at least one tag, which can be used to specify running a group of tasks, one task, or skipping a task.

For example, in the common role there are four tasks tagged as 'configure'.  Each of the four is listed below with the individual tag

- Resolv configuration - tag: resolvconf
- SSHD configuration - tag: sshdconf
- Upload issue banner - tag: issuebanner
- Sysctl config - tag: sysctlconf

If you ran the following command, all four of the above tasks will be executed against all nodes in UAT
```
ansible-playbook -i hosts/uat/inventory common.yml --tags configure
```

If you only want to run the SSHD configuration task than you would run
```
ansible-playbook -i hosts/uat/inventory common.yml --tags sshdconf
```

If, however, you want to run all the common tasks *except* the resolvconf you would run
```
ansible-playbook -i hosts/uat/inventory common.yml --skip-tags resolvconf
```

#### Templates
We are using Jinja2 templates which are written in python, enabling us to use dynamic templates, reducing the amount of files required.

The `--tags` and `--skip-tags` options both accept a comma seperated list.

## CONTRIBUTING
Contribution is encouraged to improve the repo.

You can clone the repo using the following command
```
git clone https://gitlab.com/pyratebeard/hydransible.git
```

## LINKS

* 'Managing multiple environments': https://rock-it.pl/managing-multiple-environments-with-ansible-best-practices/
* 'An Ansible2 tutorial': https://serversforhackers.com/c/an-ansible2-tutorial
* 'Ansible coding standards': https://github.com/skyzyx/ansible-coding-standards

